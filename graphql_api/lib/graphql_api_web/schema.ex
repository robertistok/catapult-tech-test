defmodule GraphqlApiWeb.Schema do
  use Absinthe.Schema

  alias GraphqlApiWeb.Resolvers
  # import Types
  import_types(GraphqlApiWeb.Schema.Types)

  query do
    field :shifts, list_of(:shift_type) do
      arg(:job_type, :string)
      arg(:start_time, :string)
      resolve(&Resolvers.ShiftResolver.shifts/3)
    end

    field :invited_contracts, list_of(:invited_contract_type) do
      arg(:role_id, :integer)
      resolve(&Resolvers.InvitedContractResolver.invited_contracts/3)
    end
  end
end
