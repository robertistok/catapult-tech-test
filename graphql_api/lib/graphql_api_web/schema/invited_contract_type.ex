defmodule GraphqlApiWeb.Schema.Types.InvitedContractType do
  use Absinthe.Schema.Notation

  object :invited_contract_type do
    field(:id, :integer)
    field(:role_id, :integer)
    field(:candidate_name, :string)
  end
end
