defmodule GraphqlApiWeb.Schema.Types.ShiftType do
  use Absinthe.Schema.Notation

  alias GraphqlApiWeb.Resolvers

  object :shift_type do
    field(:role_id, :integer)
    field(:shift_date, :string)
    field(:start_time, :string)
    field(:end_time, :string)
    field(:staff_required, :integer)
    field(:number_of_invited_staff, :integer)
    field(:job_type, :string)

    field(:candidates, list_of(:invited_contract_type),
      resolve: fn parent, args, resolution ->
        Resolvers.InvitedContractResolver.invited_contracts(
          parent,
          %{role_id: resolution.source.role_id},
          resolution
        )
      end
    )
  end
end
