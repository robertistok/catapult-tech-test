defmodule GraphqlApiWeb.Resolvers.ShiftResolver do
  @shifts_data [
    %{
      role_id: 2,
      shift_date: "Thursday 12 March 2020",
      start_time: "09:00",
      end_time: "17:00",
      staff_required: 2,
      number_of_invited_staff: 2,
      job_type: "Retail store shift"
    },
    %{
      role_id: 3,
      shift_date: "Thursday 12 March 2020",
      start_time: "13:00",
      end_time: "19:00",
      staff_required: 1,
      number_of_invited_staff: 2,
      job_type: "Waiting staff"
    },
    %{
      role_id: 4,
      shift_date: "Friday 13 March 2020",
      start_time: "10:00",
      end_time: "18:00",
      staff_required: 3,
      number_of_invited_staff: 0,
      job_type: "Barista"
    },
    %{
      role_id: 5,
      shift_date: "Saturday 14 March 2020",
      start_time: "15:00",
      end_time: "19:00",
      staff_required: 2,
      number_of_invited_staff: 1,
      job_type: "Receptionist"
    },
    %{
      role_id: 6,
      shift_date: "Saturday 14 March 2020",
      start_time: "10:00",
      end_time: "16:00",
      staff_required: 2,
      number_of_invited_staff: 0,
      job_type: "Security"
    }
  ]
  def shifts(_parent, args, _resolution) do
    job_type =
      Map.has_key?(args, :job_type) and args[:job_type] != nil and
        String.downcase(args[:job_type])

    start_time =
      Map.has_key?(args, :start_time) and args[:start_time] != nil and
        String.downcase(args[:start_time])

    res =
      @shifts_data
      |> Enum.filter(fn x ->
        if job_type,
          do:
            String.contains?(
              String.downcase(x[:job_type]),
              job_type
            ),
          else: true
      end)
      |> Enum.filter(fn x ->
        if start_time && String.match?(start_time, ~r/(am|pm)\b/),
          do:
            (
              [hours, _] = String.split(x[:start_time], ":")
              [hours_parsed, _] = Tuple.to_list(Integer.parse(hours))

              if start_time == "am" do
                hours_parsed < 12 || hours_parsed == 24
              else
                hours_parsed >= 12
              end
            ),
          else: true
      end)

    {:ok, res}
  end
end
