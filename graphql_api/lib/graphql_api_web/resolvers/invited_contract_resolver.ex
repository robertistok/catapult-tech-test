defmodule GraphqlApiWeb.Resolvers.InvitedContractResolver do
  @invited_contracts_data [
    %{
      id: 1,
      role_id: 1,
      candidate_name: 'Joe Smith'
    },
    %{
      id: 2,
      role_id: 1,
      candidate_name: 'Samantha Brown'
    },
    %{
      id: 3,
      role_id: 1,
      candidate_name: 'John Doe'
    },
    %{
      id: 4,
      role_id: 2,
      candidate_name: 'Frank Jackson'
    },
    %{
      id: 5,
      role_id: 2,
      candidate_name: 'Freddie Mercury'
    },
    %{
      id: 6,
      role_id: 3,
      candidate_name: 'Joe Smith'
    },
    %{
      id: 7,
      role_id: 3,
      candidate_name: 'Samantha Brown'
    },
    %{
      id: 8,
      role_id: 5,
      candidate_name: 'Kevin Edwards'
    }
  ]

  def invited_contracts(_parent, args, _resolution) do
    res =
      @invited_contracts_data
      |> Enum.filter(fn x ->
        if Map.has_key?(args, :role_id),
          do: x[:role_id] == args[:role_id],
          else: true
      end)

    {:ok, res}
  end
end
