defmodule GraphqlApiWeb.Router do
  use GraphqlApiWeb, :router

  pipeline :api do
    plug(CORSPlug, origin: "http://localhost:8080")
    plug(:accepts, ["json"])
  end

  scope "/api" do
    pipe_through(:api)

    forward("/graphql", Absinthe.Plug, schema: GraphqlApiWeb.Schema, json_codec: Jason)

    if Mix.env() == :dev do
      forward("/graphiql", Absinthe.Plug.GraphiQL, schema: GraphqlApiWeb.Schema, json_codec: Jason)
    end
  end
end
