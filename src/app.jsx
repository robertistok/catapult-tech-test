import { hot } from 'react-hot-loader'
import React from 'react'
import { ApolloProvider } from '@apollo/react-hooks'

import client from './lib/graphql/client'
import ShiftsList from './features/shifts/ShiftsList.jsx'
import { ShiftsStateProvider } from './state/shifts'

function App() {
  return (
    <ApolloProvider client={client}>
      <ShiftsStateProvider>
        <ShiftsList />
      </ShiftsStateProvider>
    </ApolloProvider>
  )
}

export default hot(module)(App)
