import React from 'react'
import InvitedContractsContainer from '../invitedContracts/InvitedContractsContainer'

import ShiftsFilter from './ShiftsFilter'
import { useShiftsStateValue } from '../../state/shifts'

function ShiftsList() {
  const { loading, shiftsListFiltered } = useShiftsStateValue()

  if (loading) {
    return <h2>Loading</h2>
  }

  return (
    <div>
      <h1>Shifts List</h1>

      <ShiftsFilter />

      {shiftsListFiltered.length > 0 ? (
        shiftsListFiltered.map(shift => (
          <div key={shift.roleId}>
            Job type: {shift.jobType} <br />
            Start date: {shift.shiftDate} <br />
            Start time: {shift.startTime} <br />
            End time: {shift.endTime} <br />
            <InvitedContractsContainer roleId={shift.roleId} />
            <hr />
          </div>
        ))
      ) : (
        <span>No shifts matching your criteria..</span>
      )}
    </div>
  )
}

export default ShiftsList
