import React from 'react'

import { START_TIME_PERIOD } from '../../lib/constants.js'
import { useShiftsStateValue } from '../../state/shifts'

function ShiftsFilter() {
  const { filter, updateFilter } = useShiftsStateValue()

  const handleFilterChange = e => {
    const { name, value } = e.target
    updateFilter({ [name]: value })
  }

  return (
    <div style={{ margin: '20px 0px' }}>
      <div>
        <label htmlFor='jobType'>
          Job type
          <input
            id='jobType'
            name='jobType'
            type='text'
            value={filter.jobType}
            onChange={handleFilterChange}
            placeholder='Filter by job type'
          />
        </label>
      </div>
      <div>
        Start time
        <label htmlFor={START_TIME_PERIOD.ALL}>
          <input
            defaultChecked
            type='radio'
            id={START_TIME_PERIOD.ALL}
            name='startTime'
            value={START_TIME_PERIOD.ALL}
            onChange={handleFilterChange}
          />
          All
        </label>
        <label htmlFor={START_TIME_PERIOD.AM}>
          <input
            type='radio'
            id={START_TIME_PERIOD.AM}
            name='startTime'
            value={START_TIME_PERIOD.AM}
            onChange={handleFilterChange}
          />
          AM
        </label>
        <label htmlFor={START_TIME_PERIOD.PM}>
          <input
            type='radio'
            id={START_TIME_PERIOD.PM}
            name='startTime'
            value={START_TIME_PERIOD.PM}
            onChange={handleFilterChange}
          />
          PM
        </label>
      </div>
    </div>
  )
}

export default ShiftsFilter
