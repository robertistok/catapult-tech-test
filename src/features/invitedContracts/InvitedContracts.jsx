import React from 'react'
import PropTypes from 'prop-types'

import Candidate from './Candidate'

function InvitedContracts(props) {
  const {
    fetchInvitedCandidates,
    fetchInvitedCandidatesLoading,
    fetchInvitedCandidatesCalled,
    candidates,
  } = props

  return (
    <div>
      <button
        disabled={fetchInvitedCandidatesLoading}
        onClick={fetchInvitedCandidates}
      >
        Invited Candidates
      </button>
      {fetchInvitedCandidatesCalled &&
        !fetchInvitedCandidatesLoading &&
        (candidates.length > 0 ? (
          <ul>
            {candidates.map(c => (
              <Candidate key={c.id} name={c.candidateName} />
            ))}
          </ul>
        ) : (
          <div>This shift has no candidates invited yet!</div>
        ))}
    </div>
  )
}

InvitedContracts.defaultProps = {
  candidates: [],
}

InvitedContracts.propTypes = {
  candidates: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      roleId: PropTypes.number.isRequired,
      candidateName: PropTypes.string.isRequired,
    }),
  ),
  fetchInvitedCandidates: PropTypes.func.isRequired,
  fetchInvitedCandidatesCalled: PropTypes.bool.isRequired,
  fetchInvitedCandidatesLoading: PropTypes.bool.isRequired,
}

export default InvitedContracts
