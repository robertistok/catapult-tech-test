import React from 'react'
import PropTypes from 'prop-types'
import { useLazyQuery } from '@apollo/react-hooks'
import { gql } from 'apollo-boost'

import InvitedContracts from './InvitedContracts.jsx'

const GET_INVITED_CANDIDATES = gql`
  query getInvitedCandidates($roleId: Int!) {
    invitedContracts(roleId: $roleId) {
      candidateName
      roleId
      id
    }
  }
`

function InvitedContractsContainer(props) {
  const { roleId } = props

  const [
    fetchInvitedCandidates,
    { called, loading, data = { invitedContracts: [] } },
  ] = useLazyQuery(GET_INVITED_CANDIDATES, {
    variables: { roleId },
  })

  return (
    <InvitedContracts
      fetchInvitedCandidatesCalled={called}
      fetchInvitedCandidatesLoading={loading}
      fetchInvitedCandidates={fetchInvitedCandidates}
      candidates={data.invitedContracts}
    />
  )
}

InvitedContractsContainer.propTypes = {
  roleId: PropTypes.number.isRequired,
}

export default InvitedContractsContainer
