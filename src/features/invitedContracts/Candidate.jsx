import React from 'react'
import PropTypes from 'prop-types'

function Candidate(props) {
  const { name } = props

  return <li>{name}</li>
}

Candidate.propTypes = {
  name: PropTypes.string.isRequired,
}

export default Candidate
