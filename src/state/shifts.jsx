import React, { createContext, useContext, useState, useEffect } from 'react'
import { gql } from 'apollo-boost'
import { useQuery } from '@apollo/react-hooks'
import PropTypes from 'prop-types'

import { START_TIME_PERIOD } from '../lib/constants.js'

const GET_SHIFTS_QUERY = gql`
  query getInvitedCandidates($jobType: String, $startTime: String) {
    shifts(jobType: $jobType, startTime: $startTime) {
      roleId
      shiftDate
      startTime
      endTime
      staffRequired
      numberOfInvitedStaff
      jobType
    }
  }
`

const filterByType = type => item =>
  !type || item.jobType.toLowerCase().includes(type.toLowerCase())

const filterByPeriod = ({ period, periodKey = 'startTime' }) => item => {
  if (!period || period === START_TIME_PERIOD.ALL) {
    return true
  }

  const hours = Number(item[periodKey].split(':')[0])

  return period === START_TIME_PERIOD.AM
    ? hours < 12 || hours === 24
    : hours > 12
}

const useShiftsState = () => {
  const { loading, data } = useQuery(GET_SHIFTS_QUERY)

  const [filter, setFilter] = useState({
    jobType: '',
    startTime: START_TIME_PERIOD.ALL,
  })
  const [shiftsListFiltered, setShiftsListFiltered] = useState([])
  const [shiftsList, setShiftsList] = useState([])

  useEffect(() => setShiftsList(data ? data.shifts : []), [data])

  useEffect(() => {
    const { jobType, startTime } = filter
    setShiftsListFiltered(
      shiftsList
        .filter(filterByType(jobType))
        .filter(filterByPeriod({ period: startTime, periodKey: 'startTime' })),
    )
  }, [shiftsList, filter])

  const updateFilter = updatedValue => setFilter({ ...filter, ...updatedValue })

  return { shiftsListFiltered, filter, updateFilter, loading }
}

export const ShiftsStateContext = createContext()

export const ShiftsStateProvider = ({ children }) => (
  <ShiftsStateContext.Provider value={useShiftsState()}>
    {children}
  </ShiftsStateContext.Provider>
)

ShiftsStateProvider.propTypes = {
  children: PropTypes.element.isRequired,
}

export const useShiftsStateValue = () => useContext(ShiftsStateContext)
